# Instalação

1. Rodar `npm install` na pasta do projeto
2. Adicionar o `./node_modules/.bin` no `PATH` (eu coloco direto no meu `.bash_profile`)


# Desenvolvimento

1. Rodar `cake watch` para compilar os arquivos coffee em tempo real
2. Iniciar o servidor com `node build/server.js`

# Produção

http://topdown-example.herokuapp.com/
